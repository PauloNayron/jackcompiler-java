package br.ufma.ecp;

import static br.ufma.ecp.token.TokenType.*;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufma.ecp.token.Token;
import br.ufma.ecp.token.TokenType;

public class Scanner {
    private byte[] input;
    private int current;
    private int start;

    private Boolean comments = false;

    private static final Map<String, TokenType> keywords;

    static {
        keywords = new HashMap<>();
        keywords.put("class", CLASS);
        keywords.put("constructor", CONSTRUCTOR);
        keywords.put("function", FUNCTION);
        keywords.put("method", TokenType.METHOD);
        keywords.put("field", FIELD);
        keywords.put("static", STATIC);
        keywords.put("var", VAR);
        keywords.put("int", INT);
        keywords.put("char", CHAR);
        keywords.put("boolean", BOOLEAN);
        keywords.put("void", VOID);
        keywords.put("true", TRUE);
        keywords.put("false", FALSE);
        keywords.put("null", NULL);
        keywords.put("this", THIS);
        keywords.put("let", LET);
        keywords.put("do", DO);
        keywords.put("if", TokenType.IF);
        keywords.put("else", ELSE);
        keywords.put("while", TokenType.WHILE);
        keywords.put("return", RETURN);
    }

    public Scanner(byte[] input) {
        this.input = input;
        current = 0;
        start = 0;
    }

    private void skipWhitespace() {
        char ch = peek();
        while (ch == ' ' || ch == '\r' || ch == '\t' || ch == '\n') {
            advance();
            ch = peek();
        }
    }

    private void skipComments() {
        this.skipWhitespace();
        if (peek() == '/' && peekNext() == '/') {
            while (peekNext() != '\n') {
                advance();
            }
        } else if (peek() == '/' && peekNext() == '*') {
            while (!(peek() == '*' && peekNext() == '/')) {
                advance();
            }
        } else {
            return;
        }
        advance();
        advance();
        this.skipComments();
    }

    public Token nextToken() {
        skipWhitespace();
        skipComments();

        start = current;
        char ch = peek();

        if (Character.isDigit(ch)) {
            return number();
        }

        if (isAlpha(ch)) {
            return identifier();
        }

        switch (ch) {
            case '+':
                advance();
                return new Token(PLUS, "+");
            case '-':
                advance();
                return new Token(MINUS, "-");
            case '"':
                return string();
            case 0:
                return new Token(EOF, "EOF");
            default:
                advance();
                return new Token(ILLEGAL, Character.toString(ch));
        }
    }

    private Token identifier() {
        while (isAlphaNumeric(peek())) advance();

        String id = new String(input, start, current - start, StandardCharsets.UTF_8);
        TokenType type = keywords.get(id);
        if (type == null) type = IDENT;
        return new Token(type, id);
    }

    private Token number() {
        while (Character.isDigit(peek())) {
            advance();
        }

        String num = new String(input, start, current - start, StandardCharsets.UTF_8);
        return new Token(NUMBER, num);
    }

    private Token string() {
        advance();
        start = current;
        while (peek() != '"' && peek() != 0) {
            advance();
        }
        String s = new String(input, start, current - start, StandardCharsets.UTF_8);
        Token token = new Token(TokenType.STRING, s);
        advance();
        return token;
    }

    private void advance() {
        char ch = peek();
        if (ch != 0) {
            current++;
        }
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || Character.isDigit((c));
    }

    private char peek() {
        if (current < input.length) return (char) input[current];
        return 0;
    }

    private char peekNext() {
        int next = current + 1;
        if (next < input.length) return (char) input[next];
        return 0;
    }

}
